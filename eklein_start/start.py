import sqlite3
import sys

# Connect with Database
conn = None
try:
    conn = sqlite3.connect('test.db')
    
    
# Connection failed    
except sqlite.Error, e:
    print "Error connecting to Database: %s:" % e.args[0]
    sys.exit(1)
    
curs = conn.cursor()
# curs.execute("CREATE TABLE Cars (Id INT, Name TEXT, Price INT)")
# curs.execute("INSERT INTO Cars VALUES(1,'Audi',52642)")
curs.execute("SELECT Price FROM Cars")
data = curs.fetchone()
print data

# Close Database connection
finally:
   if conn:
       conn.close()